import React from 'react';
import {render} from 'react-dom'
import './index.css';
import PermanentDrawerLeft from './components/PermanentDrawerLeft'
import {BrowserRouter, Route, Switch} from 'react-router-dom';

render(
    <div>
        <BrowserRouter>
            <Switch>
                <Route 
                    path="/searchAccount" 
                    render={(props) => 
                    <PermanentDrawerLeft {...props} 
                        displayListCompany={false} 
                        displayListCompanyForAccount={true}
                    />}
                />
                
                <Route 
                    path="/" 
                    render={(props) => 
                    <PermanentDrawerLeft {...props} 
                        displayListCompany={true} 
                        displayListCompanyForAccount={false}
                    />}
                />

            </Switch>
        </BrowserRouter>
    </div>,
    document.getElementById('root')
)
