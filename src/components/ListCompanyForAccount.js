import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import './ListCompanyForAccount.css';
import CompramateCompany from './CompramateCompany';

var accountMap = new Map();
accountMap.set('test@example.ru', ['000webhost', '126', '17Media', '17173', '2844Breaches', '2fast4u'])

function getListNames(allBreaches) {
    return allBreaches.map(breach => {
        return breach.Name;
    });
}

function getCompramateCompany(filterStr, listSelectNames) {

    if(!filterStr)
        return null;

    const accountArray = accountMap.get(filterStr);
    let compramateCompany = [];

    if(listSelectNames.size === 0 && accountArray){
        compramateCompany = accountArray;
    }
    
    if(accountArray && listSelectNames.size > 0) {
        accountArray.forEach(element => {
          if(listSelectNames.has(element))
            compramateCompany.push(element);
        });
    }

    return compramateCompany;
}

class ListCompanyOfSearchAccount extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listNames: getListNames(this.props.allBreaches),
            listSelectNames: new Set(),
            filterStr: props.filterStr,
        }
    }

    handleChange = changeValue => () => {
        const listSelectNames = this.state.listSelectNames;

        if(listSelectNames.has(changeValue.name))
            listSelectNames.delete(changeValue.name);
        else
            listSelectNames.add(changeValue.name);
        
        this.setState({listSelectNames: listSelectNames});
    }

    render() {
        const listNames = this.state.listNames;

        const { displayCompramateCompany, filterStr } = this.props;

        if(displayCompramateCompany && filterStr) {
            const compramateCompany = getCompramateCompany(filterStr, this.state.listSelectNames);
            return <CompramateCompany compramateCompany={compramateCompany}/>;
        }

        return (
            <div>
                <p className='chooseCompany'>Выберете компании (по умолчанию будет поиск по всем):</p>
                <div className='listNamesParent'>
                    {listNames.map((name, index) => (
                        <FormControlLabel
                        className = 'checkboxWithName'
                        key={index}
                        control={
                        <Checkbox
                            onChange={this.handleChange({name})}
                            color="primary"
                        />
                        }
                        label={name}
                    />
                    ))}
                </div>
            </div>
        )
    }
}

export default ListCompanyOfSearchAccount;