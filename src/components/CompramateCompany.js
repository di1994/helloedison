import React from 'react';

class CompramateCompany extends React.Component {

    render() {

        const compramateCompany = this.props.compramateCompany;
        
        const title = (compramateCompany.length === 0) ? 
            "Логин не найден ни в одной компании" : 
            "Список скомпроментированных компаний:";

        return (
            <div>
                <b>{title}</b>
                <div className='compramateCompanyParent'>
                    {compramateCompany.map((name, index) => (
                        <p key={index} className='company'>{name}</p>
                    ))}
                </div>
            </div>
        )
    }
}

export default CompramateCompany;