import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles/colorManipulator';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';

const drawerWidth = 240;

const styles = theme => ({
    appBar: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
      },
    toolbar: theme.mixins.toolbar,
    search: {
        display: 'flex',
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
          backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: '10px'
      },
      searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      inputRoot: {
        color: 'inherit',
        width: '100%',
      },
      inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          width: 220
        },
      },
});

class ApplicationBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            filterStr: '',
        }
    }

    handleChange = () => event => {
        this.setState({filterStr: event.target.value.toLowerCase()})
    };

    callBackParent = () => {
        this.props.search(this.state.filterStr);
    }

    render() {

        const { classes } = this.props;

        return (
            <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
              <div className={classes.search}>
                <InputBase
                  placeholder="Search…"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                  onChange={this.handleChange(this)}
                />
                <IconButton 
                  color="inherit"
                  onClick={this.callBackParent}>
                  <SearchIcon />
                </IconButton>
              </div>
            </Toolbar>
          </AppBar>
        )
    }
}

export default withStyles(styles)(ApplicationBar);