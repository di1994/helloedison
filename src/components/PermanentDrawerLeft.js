import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import ListCompanyTable from './ListCompanyTable';
import ListCompanyForAccount from './ListCompanyForAccount';
import ApplicationBar from './ApplicationBar';
import { Redirect } from 'react-router-dom';
import LeftMenu from './LeftMenu';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
  toolbar: theme.mixins.toolbar,
});



class PermanentDrawerLeft extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      breaches: null,
      allBreaches: null,
      displayListCompany: props.displayListCompany,
      displayListCompanyForAccount: props.displayListCompanyForAccount,
      displayCompramateCompany : false,
    }
    this.search = this.search.bind(this);
    this.itemClick = this.itemClick.bind(this);
  }

  search(filterStr) {

    if(this.state.displayListCompany)
      this.setState({resource: filterStr});
    else
      this.setState({login: filterStr, displayCompramateCompany: true});
  }

  itemClick(stateChanger) {
    this.setState(stateChanger);
  }

  render() {

    const { classes } = this.props;

    const breaches = this.state.breaches;
    if (!breaches) return <div>Loading...</div>

    return (
      <div className={classes.root}>
        {this.state.displayListCompany && this.props.location.pathname !== '/' && <Redirect to='/' />}
        {this.state.displayListCompanyForAccount && this.props.location.pathname !== '/searchAccount' && <Redirect to='/searchAccount' />}
        <CssBaseline />

        <ApplicationBar  search={this.search}/>

        <LeftMenu itemClick={this.itemClick}/>

        <main className={classes.content}>

          <div className={classes.toolbar} />

          {this.state.displayListCompany && 
            <ListCompanyTable 
              allBreaches={this.state.allBreaches}
              filterStr={this.state.resource}
            />
          }
          {this.state.displayListCompanyForAccount && 
            <ListCompanyForAccount 
              allBreaches={this.state.allBreaches} 
              displayCompramateCompany={this.state.displayCompramateCompany}
              filterStr={this.state.login}
            />
          }
        </main>
      </div>
    );
  }

  componentDidMount() {
    const URL = "https://haveibeenpwned.com/api/v2/breaches";

    fetch(URL).then(res => res.json()).then(json => {
      this.setState({ allBreaches: json, breaches: json });
    });
  }
}

export default withStyles(styles)(PermanentDrawerLeft);