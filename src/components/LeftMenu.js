import React from 'react';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
});

class LeftMenu extends React.Component {

    itemClick = index => () => {
        let stateChanger = null;

        switch (index) {
          case 0:
            stateChanger = {
              displayListCompanyForAccount: false,
              displayCompramateCompany: false,
              displayListCompany: true
            }
            break;
          case 1:
            stateChanger = {
              displayListCompany: false,
              displayCompramateCompany: false,
              displayListCompanyForAccount: true,
            }
            break;
        }

        this.props.itemClick(stateChanger);
    }


    render() {
        const { classes } = this.props;

        return (
            <div>
                <Divider />
                <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
                anchor="left"
                >
                    <Divider />
                    <List>
                        {['Поиск сайта', 'Поиск учетной записи'].map((text, index) => (
                        <ListItem
                            button
                            key={text}
                            onClick={this.itemClick(index)}>
                            <ListItemText primary={text} />
                        </ListItem>
                        ))}
                    </List>
                    <Divider />
                </Drawer>
            </div>
        )
    }
}

export default withStyles(styles)(LeftMenu);