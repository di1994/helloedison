import React from 'react';
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import PropTypes from "prop-types";
import Image from 'react-image-resizer';
import './ModalBody.css';

function fillCompromasedDataAsString(compromisedData) {

    return compromisedData.reduce(function(sum, current) {
        if(sum.length > 1)
            sum += ', ';
        return sum + current;
    }, '');
}

class ModalBody extends React.Component {

    constructor(props) {
        super(props);
        const result = fillCompromasedDataAsString(props.currentRow.DataClasses);
        this.state = {compromisedData: result};
    }

    render() {
        const { classes, currentRow } = this.props;

        const classNames = classes.paper + ' main-parent';

        return (
            <div className={classNames} tabIndex="-1">
                <FormControl className={classes.formControl} component="fieldset">
                    <FormGroup className='form-group-parent'>
                        <Image
                            src={currentRow.LogoPath}
                            height={100}
                            width={100}
                        />
                        <FormGroup className='description'>
                            <p className='whiteText title'>{currentRow.Name}</p>
                            <p className='whiteText anotherText' dangerouslySetInnerHTML={{__html: currentRow.Description}} />
                            <p className='whiteText anotherText'>
                                <b>Compromised Data: </b>
                                {this.state.compromisedData}
                            </p>
                        </FormGroup>
                    </FormGroup>
                </FormControl>
            </div >
        )
    }
}

ModalBody.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default ModalBody;