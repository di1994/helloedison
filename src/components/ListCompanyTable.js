import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ModalBody from './ModalBody';
import Modal from "@material-ui/core/Modal";

const styles = theme => ({
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
    },
});

function filterBreaches(filterStr, allBreaches) {

    if(!filterStr){
        return allBreaches;
    }
    
    return allBreaches.filter(breach => {
        return breach.Name.toLowerCase().match(filterStr) || breach.Domain.toLowerCase().match(filterStr);
    });
}

class TableBreaches extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            breaches: null,
            allBreaches: null,
            open: false,
        };
    }

    openModal = currentRow => () => {
        this.setState({ currentRow: currentRow });
        this.setState({ open: true });
    }

    closeModal = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, allBreaches, filterStr } = this.props;

        const breaches = filterBreaches(filterStr, allBreaches);

        return (
            <Paper className={classes.root}>

                <Modal
                    open={this.state.open}
                    onClose={this.closeModal}
                >
                    <ModalBody
                        classes={classes}
                        currentRow={this.state.currentRow}
                    />
                </Modal>

                <div>
                    <Table aria-labelledby="tableTitle">
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell align="center">Title</TableCell>
                                <TableCell align="center">Domain</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {breaches.map((row, index) => (
                                <TableRow key={index} hover onClick={this.openModal(row)}>
                                    <TableCell>
                                        {row.Name}
                                    </TableCell>
                                    <TableCell align="center">
                                        {row.Title}
                                    </TableCell>
                                    <TableCell align="center">
                                        {row.Domain}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </div>
            </Paper>
        )
    }
}

TableBreaches.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TableBreaches);